package com.travactory.recruitment.junior.controller;

import com.travactory.recruitment.junior.model.BookingPrice;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.travactory.recruitment.junior.model.Booking;
import com.travactory.recruitment.junior.model.FlightDuration;
import com.travactory.recruitment.junior.service.BookingsService;

@RestController
@RequestMapping("/api/bookings")
public class BookingsController {

    private BookingsService bookingsService;

    public BookingsController(final BookingsService bookingsService) {this.bookingsService = bookingsService;}

    @GetMapping("{id}")
    public ResponseEntity<?> getBookingById(@PathVariable(name = "id") final Integer id) {
        final Booking booking = bookingsService.getBookingById(id);
        return ResponseEntity.ok(booking);
    }

    @GetMapping("{id}/duration")
    public ResponseEntity<?> calculateDuration(@PathVariable(name = "id") final Integer id) {
        final FlightDuration flightDuration = bookingsService.calculateFlightDuration(id);
        return ResponseEntity.ok(flightDuration);
    }

    @GetMapping("{id}/price")
    public ResponseEntity<?> calculatePrice(@PathVariable(name = "id") final Integer id) {
        final BookingPrice bookingPrice = bookingsService.calculateBookingPrice(id);
        return ResponseEntity.ok(bookingPrice);
    }
}
