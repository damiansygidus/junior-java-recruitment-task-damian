package com.travactory.recruitment.junior.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "bookings")
public class Booking {

    @Id
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "departure_airport_id")
    private Airport departure;
    @ManyToOne
    @JoinColumn(name = "destination_airport_id")
    private Airport destination;
    private Date departureDate;
    private Date arrivalDate;
    private Integer customerId;
    private Double passengerPrice;
    private Integer passengers;
    private String classType;

    public Integer getId() {
        return id;
    }

    public Airport getDeparture() {
        return departure;
    }

    public Airport getDestination() {
        return destination;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public Double getPassengerPrice() {
        return passengerPrice;
    }

    public Integer getPassengers() {
        return passengers;
    }

    public String getClassType() {
        String classTypeFullName;
        switch (classType) {
            case "F":
                classTypeFullName = "First";
                break;
            case "E":
                classTypeFullName = "Economy";
            case "B":
                classTypeFullName = "Business";
                break;
            default:
                classTypeFullName = "Unknown class " + classType;

        }
        return classTypeFullName;
    }
}
