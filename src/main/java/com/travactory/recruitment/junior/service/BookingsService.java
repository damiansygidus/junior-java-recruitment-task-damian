package com.travactory.recruitment.junior.service;

import java.time.Duration;

import com.travactory.recruitment.junior.model.BookingPrice;
import org.springframework.stereotype.Service;

import com.travactory.recruitment.junior.model.Booking;
import com.travactory.recruitment.junior.model.FlightDuration;
import com.travactory.recruitment.junior.repository.BookingsRepository;

@Service
public class BookingsService {

    private BookingsRepository bookingsRepository;

    public BookingsService(final BookingsRepository bookingsRepository) {this.bookingsRepository = bookingsRepository;}

    public Booking getBookingById(final Integer id) {
        return bookingsRepository.findOne(id);
    }

    public FlightDuration calculateFlightDuration(final Integer bookingId) {
        final Booking booking = getBookingById(bookingId);

        final Duration duration = Duration.between(
                booking.getDepartureDate().toInstant(),
                booking.getArrivalDate().toInstant());

        return new FlightDuration(
                duration.toDays(),
                duration.toHours() % 24,
                duration.toMinutes() % 60);
    }

    public BookingPrice calculateBookingPrice(final Integer bookingId) {
        final Booking booking = getBookingById(bookingId);

        return new BookingPrice(
                booking.getPassengers(),
                booking.getPassengerPrice(),
                Double.valueOf(booking.getPassengers() * booking.getPassengerPrice().intValue()));
    }
}
